# Portfolio Architecture Examples

Diagram tool hosted online for free usage here: https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling

You can download the example files and import them into the tool using:

  **File -> Import from -> Device** 

Or you can click on the "Load Diagram" link and load the drawing directly into the tool.

Images that can be used for slides (click to enlarge an image):

## Logical diagrams

### Logical diagrams (Retail):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/logical-diagrams-retail.drawio) ]  [  [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/logical-diagrams-retail.drawio?inline=false)  ]

<img src="images/logical-diagrams/retail-supply-chain-ld.png" border="2" width="150" height="100">
<img src="images/logical-diagrams/retail-pos-ld.png" border="2" width="150" height="100">

### Logical diagrams (Payments):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/logical-diagrams-payments.drawio) ]  [  [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/logical-diagrams-payments.drawio?inline=false)  ]

<img src="images/logical-diagrams/payments-ld.png" border="2" width="150" height="100">

### Logical diagrams (Cloud-native development):
 
[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/logical-diagrams-cloud-native-development.drawio) ]  [  [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/logical-diagrams-cloud-native-development.drawio?inline=false)  ]

<img src="images/logical-diagrams/cloud-native-development-ld.png" border="2" width="150" height="100">
<img src="images/logical-diagrams/cloud-native-development-details-ld.png" border="2" width="150" height="100">

### Logical diagrams (Integrate SaaS applications):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/logical-diagrams-integrate-saas-applications.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/logical-diagrams-integrate-saas-applications.drawio?inline=false) ]

<img src="images/logical-diagrams/integrating-with-saas-applications-ld.png" border="2" width="150" height="100">
<img src="images/logical-diagrams/integrating-with-saas-applications-details-ld.png" border="2" width="150" height="100">

### Logical diagrams (Omnichannel customer experience)

[  [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/logical-diagrams-omnichannel-customer-experience.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/logical-diagrams-omnichannel-customer-experience.drawio?inline=false) ]

<img src="images/logical-diagrams/omnichannel-customer-experience-ld.png" border="2" width="150" height="100">
<img src="images/logical-diagrams/omnichannel-customer-experience-details-ld.png" border="2" width="150" height="100">

### Logical diagram (from workshop):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/example_logical_diagram.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/example_logical_diagram.drawio?inline=false) ]

<img src="images/logical-diagrams/workshop-example-ld.png" border="2" width="150" height="100">


## Schematic diagrams

### Schematic diagrams (Payments):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/schematic-diagrams-payments.drawio) ]  [  [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/schematic-diagrams-payments.drawio?inline=false)  ]

<img src="images/schematic-diagrams/payments-calculations-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/payments-immediate-payments-network-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/payments-immediate-payments-data-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/payments-anti-money-laundering-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/payments-fraud-detection-sd.png" border="2" width="150" height="100">

### Schematic diagrams (Cloud-native development):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/schematic-diagrams-cloud-native-development.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/schematic-diagrams-cloud-native-development.drawio?inline=false) ]

<img src="images/schematic-diagrams/cloud-native-development-local-containers-runtimes-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/cloud-native-development-local-containers-process-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/cloud-native-development-remote-containers-runtimes-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/cloud-native-development-remote-containers-process-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/cloud-native-development-deployment-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/cloud-native-development-deployment-enterprise-registry-sd.png" border="2" width="150" height="100">

### Schematic diagrams (Integrate SaaS applications):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/schematic-diagrams-integrate-saas-applications.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/schematic-diagrams-integrate-saas-applications.drawio?inline=false) ]

<img src="images/schematic-diagrams/saas-external-crm-integration-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/saas-external-crm-connector-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/saas-integration-3rd-party-platform-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/saas-integration-3rd-party-process-sd.png" border="2" width="150" height="100">

### Schematic diagrams (Omnichannel customer experience):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/schematic-diagrams-omnichannel-customer-experience.drawio) ]  [[Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/schematic-diagrams-omnichannel-customer-experience.drawio?inline=false) ]

<img src="images/schematic-diagrams/omnichannel-process-integration-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/omnichannel-mobile-integration-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/omnichannel-integration-service-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/omnichannel-integration-data-service-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/omnichannel-integration-3rd-party-service-sd.png" border="2" width="150" height="100">
<img src="images/schematic-diagrams/omnichannel-process-integration-3rd-party-services-sd.png" border="2" width="150" height="100">

### Schematic diagram (from workshop):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/example_schematic_diagram.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/example_schematic_diagram.drawio?inline=false) ]

<img src="images/schematic-diagrams/workshop-example-sd.png" border="2" width="150" height="100">


## Detailed diagrams

### Detailed diagrams (Payments):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/detailed-diagrams-payments.drawio) ]  [  [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/detailed-diagrams-payments.drawio?inline=false)  ]

<img src="images/detail-diagrams/payments-payments-api.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-payment-event-streams.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-validation-microservices-events.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-clearing-microservices.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-routing-microservices.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-aml-microservices.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-fraud-microservices.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-data-cache.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-payments-network.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-aml-payments-event-streams.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-aml-transaction-scoring.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-aml-aml-rules.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-fraud-detection-rules.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-aml-malicious-activity-streams.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-aml-suspicious-activity-reporting.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-aml-case-management.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-fraud-prevention-process.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-aml-kyc.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-aml-cusotmer-transation-data.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-aml-model-training-serving.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-api.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-message-queues.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-validation-microservices.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-detail-calculations-microservices.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-aggregation-microservices.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-reference-data.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-integration-microservices.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/payments-billing-systems.png" border="2" width="150" height="100">
  
### Detailed diagrams (Cloud-native development):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/detailed-diagrams-cloud-native-development.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/detailed-diagrams-cloud-native-development.drawio?inline=false) ]

<img src="images/detail-diagrams/developer-ide.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/maven-repo.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/scm-system.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/runtimes-frameworks.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/integration-frameworks.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/container-tooling.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/ci-cd-platform.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/image-registry.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/registry-management.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/s2i-workflow.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/sandbox-registry.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/enterprise-registry.png" border="2" width="150" height="100">

### Detailed diagrams (Integrate SaaS applications):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/detailed-diagrams-integrate-saas-applications.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/detailed-diagrams-integrate-saas-applications.drawio?inline=false) ]

<img src="images/detail-diagrams/external-saas-crm.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/crm-connector.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/web-app.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/api-management.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/front-end-microservices.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/process-facade-microservices.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/integration-microservices.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/integration-data-microservices.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/sso-server.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/3rd-party-platform-services.png" border="2" width="150" height="100">

### Detailed diagrams (Omnichannel customer experence):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/detailed-diagrams-omnichannel-customer-experience.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/detailed-diagrams-omnichannel-customer-experience.drawio?inline=false) ]

<img src="images/detail-diagrams/mobile-app.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/web-app2.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/api-management2.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/reverse-proxy.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/applications.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/front-end-microservices2.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/process-facade-microservices2.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/integration-microservices2.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/integration-data-microservices2.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/process-server.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/real-time-data-storage.png" border="2" width="150" height="100">
<img src="images/detail-diagrams/sso-server.png" border="2" width="150" height="100">

### Detail diagram (from workshop):

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/example_detail_diagram.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/example_detail_diagram.drawio?inline=false) ]

<img src="images/detail-diagrams/workshop-example-dd.png" border="2" width="150" height="100">


## Solution diagrams 

### IoT diagrams:

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/Asahi-OCP.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/Asahi-OCP.drawio?inline=false) ]

<img src="images/solution-diagrams/iot-logical.png" border="2" width="150" height="100">
<img src="images/solution-diagrams/iot-schematic.png" border="2" width="150" height="100">
<img src="images/solution-diagrams/iot-detail.png" border="2" width="150" height="100">

### Red Hat on Red Hat diagrams:

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/RHonRH.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/RHonRH.drawio?inline=false) ]

<img src="images/solution-diagrams/rhonrh-logical.png" border="2" width="150" height="100">
<img src="images/solution-diagrams/rhonrh-schematic.png" border="2" width="150" height="100">
<img src="images/solution-diagrams/rhonrh-schematic-provisioning.png" border="2" width="150" height="100">
<img src="images/solution-diagrams/rhonrh-detail-app.png" border="2" width="150" height="100">
<img src="images/solution-diagrams/rhonrh-detail-sysmanagement.png" border="2" width="150" height="100">

### Automated infrastructure remediation: 

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/remediation_v2.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/remediation_v2.drawio?inline=false) ]

<img src="images/solution-diagrams/remediation-logical.png" border="2" width="150" height="100">
<img src="images/solution-diagrams/remediation-network-blueprint.png" border="2" width="150" height="100">
<img src="images/solution-diagrams/remediation-dataflow-blueprint.png" border="2" width="150" height="100">
<img src="images/solution-diagrams/remediation-detail-smartmanagement.png" border="2" width="150" height="100">
<img src="images/solution-diagrams/remediation-detail-automationorchestration.png" border="2" width="150" height="100">

## Product demo diagrams

### CodeReady Containers (OCP 4.5) with Red Hat Quick Loan Bank Demo

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/crc-rhdm-quick-loan-bank-demo.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/product-demos/crc-rhdm-quick-loan-bank-demo.drawio?inline=false) ]

<img src="images/product-demo-diagrams/crc-quick-loan-bank-demo.png" border="2" width="150" height="100">

### CodeReady Containers (OCP 4.5) with Red Hat Process Automation Manager Mortgage Demo

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/crc-rhpam-mortgage-demo.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/product-demos/crc-rhpam-mortgage-demo.drawio?inline=false) ]

<img src="images/product-demo-diagrams/crc-rhpam-mortgage-demo.png" border="2" width="150" height="100">

### CodeReady Containers (OCP 4.5) with Red Hat Decision Manager Install

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/crc-rhdm-install-demo.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/product-demos/crc-rhdm-install-demo.drawio?inline=false) ]

<img src="images/product-demo-diagrams/crc-rhdm-install.png" border="2" width="150" height="100">

### CodeReady Containers (OCP 4.5) with Red Hat Process Automation Manager Install

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/crc-rhpam-install-demo.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/product-demos/crc-rhpam-install-demo.drawio?inline=false) ]

<img src="images/product-demo-diagrams/crc-rhpam-install.png" border="2" width="150" height="100">


## Community diagrams

### Camel-K event streaming hazard diagrams:

[ [Load Diagram](https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/Event-Streaming-Hazard.drawio) ]  [ [Download Diagram](https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/master/diagrams/community/Event-Streaming-Hazard.drawio?inline=false) ]

<img src="images/community-diagrams/camel-k-diagram-in-the-wild.png" border="2" width="150" height="100">

